﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Polymorphism
{
    class DbCommand
    {
        public DbConnection DbConnection { get; set; }
        public string DbInstruction { get; set; }
        public DbCommand(DbConnection dbConnnection, string dbInstruction)
        {
            if (dbConnnection == null)
                throw new ArgumentNullException();
            else
                this.DbConnection = dbConnnection;
            if (string.IsNullOrEmpty(dbInstruction))
                throw new ArgumentNullException();
            else
                this.DbInstruction = dbInstruction;
        }

        public void Execute()
        {
            DbConnection.OpenConnection();
            Console.WriteLine(DbInstruction);
            DbConnection.CloseConnection();
        }
    }
}
