﻿using System;
using System.IO;

namespace Polymorphism
{
    class Program
    {
        static void Main(string[] args)
        {
            DbCommand dbCommand = new DbCommand(new OracleConnection(@"D:\C#Polymorphism\Database\DbCommand.cs"),"Facy database commands");
            dbCommand.Execute();
        }
    }
}
