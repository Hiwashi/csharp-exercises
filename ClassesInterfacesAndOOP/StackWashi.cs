﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesInterfacesAndOOP
{
    class StackWashi
    {
        public List<Object> ObjectList { get; set; }
        public object Object { get; set; }


        public StackWashi()
        {
            this.ObjectList = new List<object>();            
        }

        public void Push(object obj)
        {
            if (obj == null)
                throw new InvalidOperationException("YOU FUCKED UP");
            else
                ObjectList.Add(obj);

        }
        public object Pop()
        {
            /*Make sure to take into account the scenario that we call the Pop() method on an empty stack. In this case, this method
should throw an InvalidOperationException.*/
            this.Object = ObjectList[ObjectList.Count - 1];
            ObjectList.Remove(this.Object);
            return this.Object;
        }
        public void Clear()
        {
            ObjectList.Clear();
        }

        
    }
}
