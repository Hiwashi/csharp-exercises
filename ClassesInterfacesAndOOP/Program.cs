﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesInterfacesAndOOP
{
    class Program
    {
        static void Main(string[] args)
        {
            var stack = new StackWashi();

            stack.Push(1);
            stack.Push(2);
            stack.Push(2);

            Console.WriteLine("Data: " + stack.Pop());
            Console.WriteLine("Data: " + stack.Pop());
            Console.WriteLine("Data: " + stack.Pop());

        }
    }
}
