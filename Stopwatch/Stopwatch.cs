﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stopwatch
{
    class Stopwatch
    {
        private DateTime timeStart { get; set; }
        private DateTime timeStop { get; set; }
        private TimeSpan timeSpan { get; set; }

        public void Start()
        {
            timeStart = DateTime.Now;
            //Console.WriteLine($"Start Time: {timeStart.Hour}:{timeStart.Minute}:{timeStart.Second}");
        }
        public void Stop()
        {
            timeStop = DateTime.Now;
            //Console.WriteLine($"Start Time: {timeStop.Hour}:{timeStop.Minute}:{timeStop.Second}");

        }

        public TimeSpan TimeSpan()
        {
            timeSpan = timeStop - timeStart;
            return timeSpan;
        }
    }
}
