﻿using System;
using System.Threading;

namespace Stopwatch
{

    class Program
    {
        static void Main(string[] args)
        {
            var stopwatch = new Stopwatch();
            do
            {
                StartCommand();
                stopwatch.Start();
                StopCommand();
                stopwatch.Stop();
                Console.WriteLine("Duration: " + stopwatch.TimeSpan());
            } while (true);

        }

        static void StartCommand()
        {
            bool invalidInput = true;
            do
            {
                Console.Write("Type \"Start\" to start the Stopwatch or \"Exit\" to quit: ");
                invalidInput = InputValidation(Console.ReadLine(), "start");
            } while (invalidInput);
        }
        static void StopCommand()
        {
            bool invalidInput = true;

            do
            {
                Console.Write("Type \"Stop\" to stop the Stopwatch or \"Exit\" to quit: ");
                invalidInput = InputValidation(Console.ReadLine(), "stop");
            } while (invalidInput);

        }

        static bool InputValidation(string input, string keyword)
        {
            if (input.ToLower() == "exit")
            {
                Environment.Exit(0);
            }
            if (String.IsNullOrEmpty(input) || input.ToLower() != keyword.ToLower())
            {
                Console.WriteLine($"\"{input}\" is not a valid input.\n");
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
