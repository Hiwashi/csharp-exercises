﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Section5ControlFlowE2
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Write a program to count how many numbers between 1 and 100 are divisible by 3 with no remainder.
             * Display the count on the console.*/

            List<int> numbersDivisibleBy3 = new List<int>();

            for (int i = 1; i <= 100; i++)
            {

                if (i % 3 == 0)
                    numbersDivisibleBy3.Add(i);

                if (i == 100)
                    Console.WriteLine($"There are {numbersDivisibleBy3.Count} numbers between 1 and 100 who are divisible by 3 with no remainder");

            }

            /*Write a program and continuously ask the user to enter a number or "ok" to exit. 
             *Calculate the sum of all the previously entered numbers and display it on the console.*/

            List<int> numbersList = new List<int>();

            string input;

            bool isTrue = true;

            while (isTrue)
            {
                Console.Write("Enter a number or type 'ok' to exit: ");
                input = Console.ReadLine().ToLower();

                if (input == "ok")
                {
                    Console.WriteLine("\nYOU EXITED SUCCESFULLY");
                    Console.WriteLine($"\nThe sum of all valid numbers is: {numbersList.Sum()}");
                    isTrue = false;
                }
                int number;

                bool conversionSuccess = Int32.TryParse(input, out number);
                if (conversionSuccess)
                    numbersList.Add(number);
            }

            /*Write a program and ask the user to enter a number.
             *Compute the factorial of the number and print it on the console. 
             *For example, if the user enters 5, the program should calculate 5 x 4 x 3 x 2 x 1 and display it as 5! = 120*/

            Console.Write("Enter a number: ");
            var inputNumber = Convert.ToInt32(Console.ReadLine());

            var factorial = 1;
            checked
            {
                for (var i = 1; i <= inputNumber; i++)
                    factorial *= i;
            }

            Console.WriteLine($"{inputNumber}! = {factorial}");

            /*Write a program that picks a random number between 1 and 10. Give the user 4 chances to guess the number. 
             * If the user guesses the number, display “You won"; otherwise, display “You lost". 
             * (To make sure the program is behaving correctly, you can display the secret number on the console first.)*/

            Random random = new Random();
            int randomNumber = random.Next(1, 10);
            int attemptsLeft = 4;


            Console.WriteLine($"Cheat: {randomNumber}");

            do
            {
                Console.Write("Pick a number between 1 and 10: ");
                int randomUserInput = Convert.ToInt32(Console.ReadLine());

                attemptsLeft--;

                if (randomNumber == randomUserInput)
                {
                    Console.WriteLine("YOU WON YAY");
                    break;
                }
                else if (attemptsLeft == 0)
                {
                    Console.WriteLine("YOU DON'T HAVE ANY ATTEMPTS LEFT, YOU LOSE");
                    break;
                }
                else
                {
                    Console.WriteLine($"Wrong! You have {attemptsLeft} left!");

                }

            } while (true);

            /*Write a program and ask the user to enter a series of numbers separated by comma. 
             *Find the maximum of the numbers and display it on the console. 
             *For example, if the user enters “5, 3, 8, 1, 4", the program should display 8.*/


            Console.WriteLine("Enter a series of numbers separated by a comma!\nE.g: “5, 3, 8, 1, 4");
            string inputString = Console.ReadLine();

            string[] stringArray = inputString.Split(",");
            int[] intArray = new int[stringArray.Length];

            for (int i = 0; i < stringArray.Length; i++)
            {
                intArray[i] = Convert.ToInt32(stringArray[i]);
            }

            Console.WriteLine(intArray.Max());
            Console.ReadLine();

        }
    }
}
