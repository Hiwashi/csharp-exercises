﻿using System;
using System.IO;

namespace Section9WorkingWithFilesE1
{
    class Program
    {
        static void Exercise1()
        {
            /*Write a program that reads a text file and displays the number of words.*/

            var path = @"D:\C#\csharp-exercises\Section9WorkingWithFilesE1\files\names.txt";

            var data = File.ReadAllLines(path);

            Console.WriteLine($"{data.Length} words in the {path} file.");
        
        }

        static void Exercise2()
        {
            /*Write a program that reads a text file and displays the longest word in the file.*/

            var longestWord = "";

            var path = @"D:\C#\csharp-exercises\Section9WorkingWithFilesE1\files\names.txt";

            var data = File.ReadAllLines(path);

            for (int i = 1; i < data.Length; i++)
            {
                if (data[i].Length > longestWord.Length)
                {
                    longestWord = data[i];
                }
            }

            Console.WriteLine($"Longest word is {longestWord}");

        }
        static void Main(string[] args)
        {
            Exercise2();
        }
    }
}
