﻿using System;

namespace Classes
{
    public class Person
    {
        public string Name;

        public void Introduce(string name)
        {
            Console.WriteLine("Hi {0}, I am {1}.", name, Name );
        }

        public Person Parse(string str)
        {
            var person = new Person();
            person.Name = str;
            return person;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var person = new Person();
            person.Name = "John";
            person.Introduce("Steve");
        }
    }
}
