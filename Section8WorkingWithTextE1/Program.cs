﻿using System;
using System.Collections.Generic;

namespace Section8WorkingWithTextE1
{
    class Program
    {
        static void Exercise1()
        {
            /*Write a program and ask the user to enter a few numbers separated by a hyphen.
             *Work out if the numbers are consecutive.
             *For example, if the input is "5-6-7-8-9" 
             *or "20-19-18-17-16", display a message: "Consecutive"; 
             *otherwise, display "Not Consecutive".*/
            bool inputInvalid = true;
            bool isConsecutive = true;
            var userInputIntList = new List<int>();

            do
            {
                Console.Write("Enter a numbers separated by a hypen: ");
                //var userInputStringArray = "0-1-2-3-4-5-6-7-8-9-10-11-12-13-14-15".Split('-');
                //var userInputStringArray = "15-14-13-12-11-10-9-8-7-6-5-4-3-2-1-0".Split('-');
                var userInputStringArray = Console.ReadLine().Split('-');

                foreach (var s in userInputStringArray)
                {
                    if (int.TryParse(s, out int result))
                    {
                        userInputIntList.Add(result);
                        inputInvalid = false;
                    }
                    else
                    {
                        Console.WriteLine($"'{s}' is not a valid number");
                        userInputIntList.Clear();
                        inputInvalid = true;
                        break;
                    }
                }
            } while (inputInvalid);

            for (int i = 1; i < userInputIntList.Count; i++)
            {
                if (userInputIntList[i] - userInputIntList[i - 1] == 1 || userInputIntList[i] - userInputIntList[i - 1] == -1)
                {
                    isConsecutive = true;
                }
                else
                {
                    isConsecutive = false;
                    break;
                }
            }

            if (isConsecutive)
                Console.WriteLine("Consecutive");
            else
                Console.WriteLine("Not Consecutive");




        }

        static void Exercise2()
        {
            /*Write a program and ask the user to enter a few numbers separated by a hyphen.
             *If the user simply presses Enter, without supplying an input, exit immediately;
             *otherwise, check to see if there are duplicates.
             *If so, display "Duplicate" on the console.*/

            var inputList = new List<string>();
            Console.WriteLine("Enter a few numbers separated by a hypen: ");            
            var userInput = Console.ReadLine();


            var inputArray = userInput.Split('-');
            foreach (var item in inputArray)
            {
                if (inputList.Contains(item))
                {
                    inputList.Clear();
                    break;
                }
                else
                {
                    inputList.Add(item);
                }
            }

            if (inputList.Count == 0)
            {
                Console.WriteLine("DUPLICATE");
            }
            else
            {
                Environment.Exit(0);
            }
        }

        static void Exercise3()
        {
            /*Write a program and ask the user to enter a time value in the 24-hour time format (e.g. 19:00). 
             *A valid time should be between 00:00 and 23:59. If the time is valid, display "Ok"; otherwise, display "Invalid Time". 
             *If the user doesn't provide any values, consider it as invalid time.*/

            Console.Write("Enter a time value in a 24-hour time format ( e.g. 19:00): ");
            var userInput = Console.ReadLine();

            if (DateTime.TryParse(userInput, out DateTime resultInput))
                Console.WriteLine("Ok");
            else
                Console.WriteLine("Invalid Time");
        }

        static void Exercise4()
        {
            /*Write a program and ask the user to enter a few words separated by a space. 
             *Use the words to create a variable name with PascalCase. 
             *For example, if the user types: "number of students", display "NumberOfStudents". 
             *Make sure that the program is not dependent on the input. 
             *So, if the user types "NUMBER OF STUDENTS", the program should still display "NumberOfStudents".*/

            Console.WriteLine("Enter a few words separated by a space: ");
            var userInput = Console.ReadLine().ToLower().Split(' ');

            foreach (var item in userInput)
            {
                var stringLenght = item.Length;

                for (int i = 0; i < stringLenght; i++)
                {
                    if (i == 0)
                        Console.Write(item[i].ToString().ToUpper());
                    else
                        Console.Write(item[i].ToString());
                }
            }

            Console.WriteLine("\n");

        }
        static void Exercise5()
        {
            /*Write a program and ask the user to enter an English word. 
             *Count the number of vowels (a, e, o, u, i) in the word. 
             *So, if the user enters "inadequate", the program should display 6 on the console.*/

            Console.Write("Enter an English word: ");
            var userInput = Console.ReadLine().ToLower();
            var charList = new List<char>();

            for (int i = 0; i < userInput.Length; i++)
            {
                charList.Add(userInput[i]);
            }

            int count = 0;

            foreach (var item in charList)
            {
                if (item == 'a' || item == 'e' || item == 'i' || item == 'o' || item == 'u' )
                    count++;
            }

            Console.WriteLine($"{userInput} has {count} vowels");
           

        }
        static void Main(string[] args)
        {
            Exercise5();
        }
    }
}
