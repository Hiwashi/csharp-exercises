﻿using System;
using System.Collections.Generic;

namespace Section6ArraysAndListsE1
{
    class Program
    {
        static void Exercise1()
        {
            /*When you post a message on Facebook, depending on the number of people who like your post, Facebook displays different information.
            If no one likes your post, it doesn't display anything.
            If only one person likes your post, it displays: [Friend's Name] likes your post.
            If two people like your post, it displays: [Friend 1] and [Friend 2] like your post.
            If more than two people like your post, it displays: [Friend 1], [Friend 2] and [Number of Other People] others like your post.
            Write a program and continuously ask the user to enter different names, until the user presses Enter (without supplying a name). 
            Depending on the number of names provided, display a message based on the above pattern.*/

            var friendsList = new List<string>();
            var doLoop = true;

            do
            {
                Console.Write("Enter a name: ");
                var userInput = Console.ReadLine();

                if (userInput == "")
                {
                    switch (friendsList.Count)
                    {
                        case 0: Console.WriteLine("TEST");
                            doLoop = false;
                            break;
                        case 1: Console.WriteLine($"{friendsList[0]} likes your post.");
                            doLoop = false;
                            break;
                        case 2: Console.WriteLine($"{friendsList[0]} and {friendsList[1]} like your post.");
                            doLoop = false;
                            break;                        
                    }
                    if (friendsList.Count >= 3)
                        Console.WriteLine($"{friendsList[0]} and {friendsList[1]} and {friendsList.Count - 2} others like your post.");
                }
                else
                {
                    friendsList.Add(userInput);
                }

            } while (doLoop);
        }

        static void Exercise2()
        {
            /*Write a program and ask the user to enter their name. 
             *Use an array to reverse the name and then store the result in a new string. 
             *Display the reversed name on the console.*/
            var stringArray = new string[1];

            Console.Write("Enter your name: ");
            var userName = Console.ReadLine();

            stringArray[0] = userName;

            var charArray = new char[userName.Length];

            for (int i = 0; i < userName.Length; i++)
            {
                charArray[i] = userName[i];
            }

            Console.Write("Reverse: ");

            for (int i = (charArray.Length - 1); i >= 0; i--)
            {
                Console.Write($"{charArray[i]}");

            }
            Console.WriteLine();
        }

        static void Exercise3()
        {
            /*Write a program and ask the user to enter 5 numbers.
             *If a number has been previously entered, display an error message and ask the user to re-try.
             *Once the user successfully enters 5 unique numbers, sort them and display the result on the console.*/

            var numberArray = new int[5];
            var validArray = 0;

            do
            {
                Console.Write("Enter a number: ");

                var numberInput = Convert.ToInt32(Console.ReadLine());

                if (Array.IndexOf(numberArray, numberInput) == -1)
                {
                    numberArray[validArray] = numberInput;
                    validArray++;
                }
                else
                {
                    Console.WriteLine($"{numberInput} already exists, pick a different number.");
                }

            } while (validArray < 5);

            Array.Sort(numberArray);

            foreach (var item in numberArray)
            {
                Console.Write(String.Format($"{item} "));
            }
            Console.WriteLine();
            

        }

        static void Exercise4()
        {
            /*Write a program and ask the user to continuously enter a number or type "Quit" to exit. 
             *The list of numbers may include duplicates. 
             *Display the unique numbers that the user has entered.*/

            var numbersList = new List<int>();
            var noRepeatList = new List<int>();
            var doLoop = true;

            do
            {
                Console.Write("Type a number or write \"Quit\" to exit: ");
                var userInput = Console.ReadLine();

                if (userInput.ToLower() == "quit")
                {
                    doLoop = false;
                }
                else
                {
                    var validInput = Int32.TryParse(userInput, out int result);
                    if (validInput)
                        numbersList.Add(result);
                    else
                        Console.WriteLine("Invalid input");
                }

            } while (doLoop);



            foreach (var item in numbersList)
            {
                if (!noRepeatList.Contains(item))
                {
                    noRepeatList.Add(item);
                }
            }

            if (numbersList.Count != 0)
            {
                Console.Write("Unique numbers entered by the user: ");
                foreach (var item in noRepeatList)
                {
                    Console.Write(String.Format($"{item} "));
                }
            }

            Console.WriteLine();


        }

        static void Exercise5()
        {
            /*Write a program and ask the user to supply a list of comma separated numbers (e.g 5, 1, 9, 2, 10).
             *If the list is empty or includes less than 5 numbers, display "Invalid List" and ask the user to re-try;
             *otherwise, display the 3 smallest numbers in the list.*/

            string userInput;

            do
            {
                Console.Write("Please supply a list of comma separated numbers (e.g 5, 1, 9, 2, 10): ");
                userInput = Console.ReadLine();
                var _stringArray = userInput.Split(",");

                if (String.IsNullOrWhiteSpace(userInput))
                {
                    Console.Clear();
                    Console.WriteLine("Invalid List");
                    Console.WriteLine("List cannot be empty, try again.\n");
                }
                else if (_stringArray.Length > 0 && _stringArray.Length < 5)
                {
                    Console.Clear();
                    Console.WriteLine("Invalid List");
                    Console.WriteLine("List must have at least 5 numbers, try again.\n");
                }

                var _intArray = Array.ConvertAll(_stringArray, Int32.Parse);


                Array.Sort(_intArray);

                Console.Write("\n\nThe 3 smallest numbers are: ");
                for (int i = 0; i <= 2; i++)
                {
                    Console.Write(_intArray[i]+ " ");
                }
                break;


            } while (true);
            Console.WriteLine();

        }
        static void Main(string[] args)
        {
            Exercise3();
        }

    }
}
