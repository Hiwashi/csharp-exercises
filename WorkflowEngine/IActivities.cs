﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkflowEngine
{
    public interface IActivities
    {
        void Execute();
    }
}
