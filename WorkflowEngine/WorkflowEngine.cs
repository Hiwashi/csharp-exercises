﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkflowEngine
{
    class WorkflowEngine
    {
        private readonly List<IActivities> ActivitiesList = new List<IActivities>();
        public void Add(IActivities activities)
        {
            ActivitiesList.Add(activities);
        }        
        public void Run()
        {
            foreach (var item in ActivitiesList)
            {
                item.Execute();
            }
        }

    }
}
