﻿using System;

namespace WorkflowEngine
{
    class Program
    {
        static void Main(string[] args)
        {
            var workflowEngine = new WorkflowEngine();

            workflowEngine.Add(new VideoUpload());
            workflowEngine.Add(new CallWebService());
            workflowEngine.Add(new SendEmail());
            workflowEngine.Add(new ChangeVideoStatus());

            workflowEngine.Run();
    
        }
    }
}
